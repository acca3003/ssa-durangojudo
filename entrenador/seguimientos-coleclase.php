<!DOCTYPE html>
<html lang="en">
<head>

<?php include './cabecera.php';?>
<?php include './seguridad.php';?>

<title> Alumnos en SSA </title>


<link rel="stylesheet" type="text/css" href="../bower_components/datatables/media/css/jquery.dataTables.min.css">
</head>

<body>

    <div id="wrapper">
<script>
console.log(<?php echo $_SESSION['uid']?>);
</script>
<?php include './navegacion.php';?>

<script>
console.log(<?php echo $_SESSION['uid']?>);
				function nuevoSeguimiento ()
				{
 
						
						var table = $('#listadoalumnos').DataTable();
						//alert(table.row('.selected').data()[0]);
						if( table.row('.selected').length > 0 )
						{
						window.location = "crear_seguimiento.php?alumno=" + encodeURIComponent(table.row('.selected').data()[0]);
   						}
						
						
														 
				}

				function verSeguimientos()
				{

										var table = $('#listadoalumnos').DataTable();
						//alert(table.row('.selected').data()[0]);
						if( table.row('.selected').length > 0 )
						{
						window.location = "ver_seguimientos.php?alumno=" + encodeURIComponent(table.row('.selected').data()[0]);
   						}					
														 
				}
				function edita ()
				{
					$('#listadoalumnos').find('tr').each(function() {
						var row = $(this);
						if (row.find('input[type="checkbox"]').is(':checked')) {
							//alert(row.find("td:nth-child(2)").text());
							//alert(row.find("td:nth-child(3)").text());
						}
					}
														 )
				}
				function elimina ()
				{
					$('#listadocolegios').find('tr').each(function() {
						var row = $(this);
						if (row.find('input[type="checkbox"]').is(':checked')) {
							// alert(row.find("td:nth-child(2)").text());
							//alert(row.find("td:nth-child(3)").text());
						}
					}
														 )
				}
			
</script>

<div id="page-wrapper">
   <div class="row">
       <div class="col-lg-12">
             <h3 class="page-header">Gestion de seguimientos del SSA</h3>
        </div>
                <!-- /.col-lg-12 -->
    </div>
           
    <div class="row">

                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Listado de Alumnos
                            <div class="pull-left">
                                <div class="btn-group">
                                    <?php include 'acciones_seguimientos.php';?>
                                </div>
                            </div>
                        </div>


            <!-- /.panel-heading -->
                        <div class="panel-body">
 
<!-- /.panel-heading -->
						<div class="row">
							<div class="col-lg-12">
								<!-- /.panel-heading -->
								<div class="dataTable_wrapper">
									<table class="table table-striped table-bordered table-hover" id="listadoalumnos">
										<thead>
											<tr>
												<th>
													#
												</th>
												<th>
													Colegio
												</th>
												<th>
													Clase
												</th>
												<th>
													Nombre
												</th>
												<th>
													Apellidos
												</th>
												<th>
													Cinturon
												</th>
												<th>
													Puntuacion
												</th>
												<th>
													Categoria
												</th>
												<th>
													Email Padres
												</th>
<th>
													Movil Padres
												</th>
											</tr>
										</thead>
										<tbody>
										
	<?php
											
	$idcole=substr($_GET["coleclase"],0,strpos($_GET["coleclase"],"/"));
	$idcla=substr($_GET["coleclase"],strpos($_GET["coleclase"],"/")+1);
	
	$conn = mysqli_connect($servername, $username, $password, $dbname,"3306");
	// Check connection
	if (!$conn) {
		die("Connection failed: " . mysqli_connect_error());
	}
												
//$sql = "SELECT * FROM ssa_alumnos";
$sql = "SELECT ssa_alumnos.idalumno, ssa_colegios.nombrecolegio, ssa_clases.nombreclase, ssa_alumnos.nombrealumno, ssa_alumnos.apellidosalumno, ssa_alumnos.cinturon, ssa_alumnos.puntuacion, ssa_alumnos.fechanacimiento, ssa_alumnos.correopadres, ssa_alumnos.movilpadres FROM ssa_alumnos LEFT JOIN ssa_colegios ON ssa_alumnos.colegio=ssa_colegios.idcolegio LEFT JOIN ssa_clases ON ssa_alumnos.clase=ssa_clases.idclase WHERE  ssa_alumnos.colegio=".$idcole." and ssa_alumnos.clase=".$idcla." ORDER BY ssa_colegios.nombrecolegio ASC, ssa_clases.nombreclase ASC, ssa_alumnos.apellidosalumno ASC";
//echo $sql;
//$result = mysqli_query($conn, "SELECT ssa_alumnos.idalumno, ssa_colegios.nombrecolegio, ssa_clases.nombreclase, ssa_alumnos.nombrealumno, ssa_alumnos.apellidosalumno, ssa_alumnos.cinturon, ssa_alumnos.puntuacion, ssa_alumnos.fechanacimiento, ssa_alumnos.correopadres, ssa_alumnos.movilpadres FROM ssa_alumnos LEFT JOIN ssa_colegios ON ssa_alumnos.colegio=ssa_colegios.idcolegio LEFT JOIN ssa_clases ON ssa_alumnos.clase=ssa_clases.idclase WHERE  ssa_alumnos.colegio=".$idcole." and ssa_alumnos.clase=".$idcla." ORDER BY ssa_colegios.nombrecolegio ASC, ssa_clases.nombreclase ASC, ssa_alumnos.apellidosalumno ASC;");
$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) > 0) {
	// output data of each row
	echo "resultados";
	while($row = mysqli_fetch_assoc($result)) {
		echo "<tr>";
		//$categoriaY= substr($row[fechanacimiento],0,4); 
		//$foo +=0;
		//echo date('Y m');
		$year=date("Y");
		$month=date("m");
		//echo $year."---".$month;
		//echo "diferencia ".$dif;
		//Hay que controlar desde el mes de octubre ya como el siguiente
		if ( $month<9 )
		{
			$dif= $year-substr($row[fechanacimiento],0,4);
		}
		else
		{
			$dif= $year-substr($row[fechanacimiento],0,4)+1;
		}
		switch ($dif) {
			case 20:
			$categoria ="junior";
			break;
			case 19:
			$categoria ="cadjuniorete";
			break;
			case 18:
			$categoria ="cajuniordete";
			break;
			case 17:
			$categoria ="cadete";
			break;
			case 16:
			$categoria ="cadete";
			break;
			case 15:
			$categoria ="cadete";
			break;
			case 14:
			$categoria ="infantil";
			break;
			case 13:
			$categoria ="infantil";
			break;
			case 12:
			$categoria ="alevin";
			break;
			case 11:
			$categoria ="alevin";
			break;
			case 10:
			$categoria ="benjamin";
			break;
			case 9:
			$categoria ="benjamin";
			break;
			default:
			$categoria ="otros";
		}
              switch ($row[cinturon]) {
			case 1:
			$cinturon="blanco";
			break;
			case 2:
			$cinturon ="blanco-amarillo";
			break;
			case 3:
			$cinturon="amarillo";
			break;
			case 4:
			$cinturon="amarillo-naranja";
			break;
			case 5:
			$cinturon="naranja";
			break;
			case 6:
			$cinturon="naranja-verde";
			break;
			case 7:
			$cinturon="verde";
			break;
			case 8:
			$cinturon="verde-azul";
			break;
			case 9:
			$cinturon="azul";
			break;
			case 10:
			$cinturon="azul-marron";
			break;
			case 11:
			$cinturon="marron";
			break;
			default:
			$cinturon="negro";
		}
		//echo "fecha ".$row[fechanacimiento];
		// echo "año ".substr($row[fechanacimiento],0,4);
		// echo "actual (desde septiembre es el siguiente)".  substr($row[fechanacimiento],0,4)
		echo "<td id=idalumno> " . $row["idalumno"]. "</td><td id=idcolegio> " . $row["nombrecolegio"]. "</td><td id=idclase> " . $row["nombreclase"]. "</td><td id=idnombrealumno>" . $row["nombrealumno"]. "</td><td id=idapellidosalumno> " . $row["apellidosalumno"]. "</td><td id=idcinturon> " . $cinturon. "</td><td id=idpuntuacion> " . $row["puntuacion"]. "<td id=idcategoria> " . $categoria. "</td><td id=idcorreopadres> ".$row["correopadres"]. "</td><td id=idmovilpadres> ".$row["movilpadres"]. "</td>";
		echo "</tr>";
	}
} else {
	//echo "0 results";
	// echo "aaaaano resultados";
}
		mysqli_close($conn);
		
		?> 


                                            </tbody>
                                        </table>									

<script>


										
	$(document).ready(function() {
    var table = $('#listadoalumnos').DataTable( {
    	 "order": [[ 1, 'asc' ], [ 2, 'asc' ], [ 4, 'asc' ]]
    }  );
 
    $('#listadoalumnos tbody').on( 'click', 'tr', function () {
		console.log("pinchado");
        if ( $(this).hasClass('selected') ) {
        	  console.log("seleccionada ya");
            $(this).removeClass('selected');
            console.log("seleccionada ya");
        }
        else {
        	  console.log("no seleccionada1");
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            console.log("no seleccionada despues");
        }
    } );
 
    
} );
									</script>
								</div>
								<!-- /.table-responsive -->
							</div>
							<!-- /.col-lg-4 (nested) -->
						</div>
						<!-- /.row -->
					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
				<!-- /.panel -->
			</div>



                        </div>
                        <!-- /.panel-body -->
 

    </div>
    <!-- /.row -->

</div>
    <!-- /#wrapper -->
    
    
 
  <?php include 'pie.php';?>  
    
    

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

  <script>
  /*  $(document).ready(function() {
        $('#listadoalumnos').DataTable({
                responsive: true
        });
    });*/
    

    </script>

</body>

</html>	
