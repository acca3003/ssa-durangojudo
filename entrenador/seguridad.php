<?php

session_start();
 
//Validar que el usuario este logueado y exista un UI y tiene el rol adecuado
if ( ! ($_SESSION['autenticado'] == 'si' && isset($_SESSION['uid']) && $_SESSION['rol'] == 'entrenador' ) )
{
    //En caso de que el usuario no este autenticado, crear un formulario y redireccionar a la 
    //pantalla de login, enviando un codigo de error
?>
        <form name="formulario" method="post" action="login.php">
            <input type="hidden" name="msg_error" value="2">
            <input type="hidden" name="url" value=<?php echo $_SERVER['REQUEST_URI'] ?>>
        </form>
        <script type="text/javascript"> 
            document.formulario.submit();
        </script>
<?php
}
  

?>  