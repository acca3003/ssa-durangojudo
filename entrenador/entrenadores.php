<!DOCTYPE html>
<html lang="en">
<head>

<?php include 'cabecera.php';?>
<?php include 'seguridad.php';?>

<title> Entrenadores en SSA </title>
<link rel="stylesheet" type="text/css" href="../bower_components/datatables/media/css/jquery.dataTables.min.css">
</head>

<body>

    <div id="wrapper">

<?php include 'navegacion.php';?>

<script>

function editarEntrenador()
{

	var table = $('#listadoentrenadores').DataTable();
	//alert(table.row('.selected').data()[0]);
	if( table.row('.selected').length > 0 )
	{
	window.location = "editar_entrenador.php?uid=" + encodeURIComponent(table.row('.selected').data()[0]);
		}	
	
}
			
</script>

<div id="page-wrapper">
   <div class="row">
       <div class="col-lg-12">
             <h1 class="page-header">Gestion de entrenadores del SSA</h1>
        </div>
                <!-- /.col-lg-12 -->
    </div>
           
    <div class="row">

                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Listado de Entrenadores
                            <div class="pull-left">
                                <div class="btn-group">
                                    <?php include 'acciones_entrenadores.php';?>
                                </div>
                            </div>
                        </div>


<?php include 'listado_entrenadores.php';?>



    </div>
    <!-- /.row -->

</div>
    <!-- /#wrapper -->

  <?php include 'pie.php';?>  


    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
  <script>

    </script>

</body>

</html>