<!DOCTYPE html>
<html lang="es">

<head>
<?php session_start(); ?>


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sistema de Seguimiento Avanzado SSA</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


  <!-- JQuery -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="../jquery/jquery-1.12.0.min.js"></script>
    <script src="../jquery/jquery.tablecheckbox.js"></script>
    
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
<script>
function mensajeAlerta()
{
 <?php
    if( isset( $_POST['msg_error'] ) )
        {
            switch( $_POST['msg_error'] )
            {
                case 1:
            ?>

            
            //alert("antes El usuario o password son incorrectos.", "Seguridad");
                        document.getElementById("textoventana").innerHTML="La seccion a la que intentaste entrar esta restringida.<br> Solo permitida para usuarios registrados.";
                          document.getElementById("ventana").click();
               //alert("despues El usuario o password son incorrectos.", "Seguridad");
                

            <?php
                break;          
                case 2:
            ?>

            //alert( "antes" );
            //alert("---La seccion a la que intentaste entrar esta restringida.\n Solo permitida para usuarios registrados.", "Seguridad");
            document.getElementById("textoventana").innerHTML="La seccion a la que intentaste entrar esta restringida.<br> Solo permitida para usuarios registrados.";
            document.getElementById("ventana").click();
            //alert( "despues" );
            //alert("<?php echo "ERROR: ".$_POST['msg_error'] ?>");
            //alert("<?php echo "URL: ".$_POST['url'] ?>");
     
            

            <?php       
                break;
            }       //Fin switch
        }

 if( $_POST['url']=='' )
        {
$url="index.php";
//echo $url
}
else
{
$url=$_POST['url'];
}
    ?>

}

</script>

</head>

<body onload="mensajeAlerta()">

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
   
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Por favor introduzca su usuario(email) y password en SSA </h3>
                    </div>
<div id="oculta" style="display: none;">
                            <button id="ventana" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal" hidden="hidden">
                                Ventana con mensajes
                            </button>
</div>



<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Aviso!</h4>
                                        </div>
                                        <div class="modal-body" id="textoventana">
                                            
                                        </div>
                                        <div class="modal-footer">
                                            <center><button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button></center>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
</div>
                                <!-- /.modal-dialog -->
                                </div>


                                
                                
                    <div class="panel-body">
                                   
                                
                        <form role="form" action="autentica_usuario.php" method="POST">
                            <fieldset>
                                <div class="form-group">
                                    <input type="hidden" name="url" value="<?php echo $url ?>">
                                    <input class="form-control" placeholder="E-mail" name="email" type="email" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="">

                                </div>
 
                                <!-- Change this to a button or input when using this as a form -->
                                <input type="submit" class="btn btn-lg btn-success btn-block">
                                
                            </fieldset>
                        </form>
                        
                        
                    </div>
                    
                    
                    
                </div>
            </div>
        </div>
    </div>




    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>		