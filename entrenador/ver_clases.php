<!DOCTYPE html>
<html lang="en">

<head>

<title> Seguimiento </title>
<?php include 'cabecera.php';?>
<?php include 'seguridad.php';?>
<link rel="stylesheet" type="text/css" href="../bower_components/datatables/media/css/jquery.dataTables.min.css">
    <link href="../pruebaflot.css" rel="stylesheet" type="text/css">
	<script src="../bower_components/jquery/dist/jquery.min.js"></script>


 <script>


	
function editarClase(){
					
 
 						var table = $('#listadoclases').DataTable();
						
						if( table.row('.selected').length > 0 )
						{
						window.location = "editar_clase.php?clase=" + encodeURIComponent(table.row('.selected').data()[0]);
   						}	
 
 
}

function eliminarSeguimiento(){
					
  
 
}


</script>
</head>

<body>

    <div id="wrapper">


<?php include 'navegacion.php';?>
    
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Clases del colegio</h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
  
                    <div class="panel panel-default">
                        <div class="panel-heading">
                              <div class="pull-left">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Acciones
                                        <span class="caret"></span>
                                    </button>
<script>



</script>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li><a href="crear_clase.php?colegio=<?php echo $_GET['colegio']?>">Nueva Clase</a>
                                        </li>
                                        <li><a href="#" onclick="editarClase()">Editar Clase</a>
                                        </li>
                                          <li><a href="#" onclick="">Eliminar Clase</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>


			<div id="divCheckbox" style="display: none;">
					<button id="ventana" class="btn btn-primary btn-lg"
						data-toggle="modal" data-target="#myModal" hidden="hidden">
						Ventana con mensajes</button>
				</div>

				<!-- /.panel-heading -->
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-8">
							<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
								aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal"
												aria-hidden="true">&times;</button>
											<h4 class="modal-title" id="myModalLabel">Aviso!</h4>
										</div>
										<div class="modal-body" id="textoventana"></div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default"
												data-dismiss="modal">Cerrar</button>
										</div>
									</div>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>
							
	
														
							







                        <!-- /.panel-heading -->
                        <div class="panel-body">
 
						<div class="row">
							<div class="col-lg-12">
								<!-- /.panel-heading -->
								<div class="dataTable_wrapper">
									<table class="table table-striped table-bordered table-hover" id="listadoclases">
										<thead>
											<tr>
												<th>
													#
												</th>
												<th>
													Nombre Clase
												</th>
												
											</tr>
										</thead>
										<tbody>
											<?php
											$conn = mysqli_connect($servername, $username, $password, $dbname,"3306");
											// Check connection
											if (!$conn) {
												die("Connection failed: " . mysqli_connect_error());
											}
//$sql = "SELECT * FROM ssa_alumnos";
//$sql = "SELECT ssa_alumnos.idalumno, ssa_colegios.nombrecolegio, ssa_clases.nombreclase, ssa_alumnos.nombrealumno, ssa_alumnos.apellidosalumno, ssa_alumnos.cinturon, ssa_alumnos.puntuacion, ssa_alumnos.fechanacimiento, ssa_alumnos.correopadres, ssa_alumnos.movilpadres FROM ssa_alumnos LEFT JOIN ssa_colegios ON ssa_alumnos.colegio=ssa_colegios.idcolegio LEFT JOIN ssa_clases ON ssa_alumnos.clase=ssa_clases.idclase";

$sql = "SELECT * FROM ssa_clases where colegio=" . $_GET['colegio'];
$result = mysqli_query($conn, $sql);


if (mysqli_num_rows($result) > 0) {
	// output data of each row
	while($row = mysqli_fetch_assoc($result)) {
		echo "<tr>";
				echo "<td id=idclase> " . $row["idclase"]. "</td><td id=idnombreclase> " . $row["nombreclase"]. "</td>";
		echo "</tr>";
	}
} else {
	//echo "0 results";
}


					
mysqli_close($conn);
?>  
										</tbody>
									</table>
									<script>
									
									$(document).ready(function() {
    var table = $('#listadoclases').DataTable();
 
    $('#listadoclases tbody').on( 'click', 'tr', function () {
		//alert("pinchado");
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
			//alert("seleccionada ya");
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
			//alert("no seleccionada antes");
        }
    } );
 
    
} );
									</script>
								</div>
								<!-- /.table-responsive -->
							</div>
							<!-- /.col-lg-4 (nested) -->
						</div>
						<!-- /.row -->
					</div>
					<!-- /.panel-body -->
					
					
									</div>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>
				</div>
				<!-- /.panel -->
				<!-- /.panel -->
			</div>



                        </div>
                        <!-- /.panel-body -->      
                   
                  
                        
                </div>
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    
      <?php include 'pie.php';?>  

    <!-- jQuery -->
    <!-- <script src="../bower_components/jquery/dist/jquery.min.js"></script> -->

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
	<!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

 
</body>

</html>