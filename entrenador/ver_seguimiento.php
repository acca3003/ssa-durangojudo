<!DOCTYPE html>
<html lang="en">

<head>
<style> 
@font-face {
    font-family: fuentedurango;
    src: url("/fonts/VAGRoundedBold.ttf");
}
</style>
<title> Seguimiento </title>
<?php include 'cabecera.php';?>
<?php include 'seguridad.php';?>

<?php

$sql = "SELECT * FROM ssa_seguimiento where idseguimiento=" . $_GET['seguimiento'];
$iseguimientod = $_GET['seguimiento'];
//echo $uid;

$result = mysqli_query($conn, $sql);

//echo $sql;

if (mysqli_num_rows($result) > 0) {
    // output data of each row
    
    while($row = mysqli_fetch_assoc($result)) {
       $idalumno=$row["idalumno"];
       $actitud=$row["actitud"];
       $comportamiento=$row["comportamiento"];
       $esfuerzo=$row["esfuerzo"];
       $participacion=$row["participacion"];
       $energia=$row["energia"];
       $fechaseguimiento=$row["fechaseguimiento"];
       $observaciones=$row["observaciones"];
       $puntosdia=$row["puntosdia"];
       $puntosextra=$row["puntosextra"];
       $puntostotales=$row["puntostotales"];
       $cinturon=$row["cinturon"];
  
    }
} else {
    // echo "0 results";
}

$sql = "SELECT * FROM ssa_alumnos where idalumno=" . $idalumno;

//echo $uid;

$result = mysqli_query($conn, $sql);

//echo $sql;

if (mysqli_num_rows($result) > 0) {
    // output data of each row
    
    while($row = mysqli_fetch_assoc($result)) {
       $nombrealumno=$row["nombrealumno"]." ".$row["apellidosalumno"];
       $colegio =  $row ["colegio"];
      
  
    }
} else {
    // echo "0 results";
}

$sql = "SELECT * FROM ssa_colegios where idcolegio=$colegio";
$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) > 0) {
	// output data of each row
	while($row = mysqli_fetch_assoc($result)) {
		$nombrecolegio = $row["nombrecolegio"];
	}
} else {
	//echo "0 results";
}
mysqli_close($conn);
?>  
 <script>

var d = new Date();
var cargado = "no";
var puntostotales=0;
var cinturon=0;
var colegio="<?php echo $nombrecolegio ?>";

function ratonCargaFondo(e) {
if( cargado == "no" )
{
       pintaFondo();
       cargado="si";
}

}
function pintaObservaciones(observaciones) {
   // control si está vacía
    var res = observaciones.split(" ");
    var texto ="";
    var texto1 = "";
    var ndemo =1;
    var canvas = document.getElementById("thecanvas");
    var ctx = canvas.getContext("2d");
    var indicelineas=0;

    // recorremos todo el array y vamos creando un string hasta tener el maximo de caracteres
    for	(index = 0; index < res.length; index++) {
         //alert( "indice " + index + "res " + res[index]);
          texto1 = texto + " " + res[index];
          if( texto1.length > 55 )
          {
               //imprimir texto y ponerlo con el actual de inicio
               //alert( texto );
                
                ctx.font = "20px fuentedurango";
                ctx.fillText(texto, 50, 735+indicelineas*20);
               texto = " " + res[index];
               indicelineas++;

          }
          else
          {
               texto = texto1;
          }
    }
                ctx.font = "20px fuentedurango";
                ctx.fillText(texto, 50, 735+indicelineas*20);

   // document.getElementById("demo").innerHTML = res;
}
function cargaInicial(){
    //cargaAlumno();
    pintaTodo();
}




function cargaAlumno()
{
<?php

       echo "document.getElementById('idnombre').value='", $alumno , " " , $apellidos  , "';";
       //echo "document.getElementById('idcinturon').value='", $cinturon , "';";
       echo "document.getElementById('idpuntuacion').value='", $puntuacion , "';";

?>  
}

 function updateTextInput(val) {
      document.getElementById('textInput').value=val; 
    }

function pintaEstrellas(n, posicion)
{

             var estrella1=document.getElementById("iduno");
             var estrella2=document.getElementById("iddos");
             var estrella3=document.getElementById("idtres");
             var estrella4=document.getElementById("idcuatro");
             var estrella5=document.getElementById("idcinco");
             var estrellas = [estrella1, estrella2, estrella3, estrella4, estrella5];
         var canvas = document.getElementById("thecanvas");
                var ctx = canvas.getContext("2d");
//alert("valor de n: " + n + " posicion: "+posicion);
 if ( colegio.toLowerCase()=="club")
                {

                }
                else
                {
             for (i = 0; i < posicion; i++) { 
                       ctx.drawImage(estrellas[i],50+i*50,238+(n-1)*84,32,30);
//alert("buclue i: " + i);
              }
                }
             

}

function pintaPilas(posicion)
{
        var canvas = document.getElementById("thecanvas");
                var ctx = canvas.getContext("2d");
             var pila=document.getElementById("idbateria");
             if ( colegio.toLowerCase()=="club")
             {

             }
             else
             {
              for (i = 0; i < posicion; i++) { 
                       ctx.drawImage(pila,50+i*50,582,32,30);
//alert("buclue i: " + i);
              }
             }
}

function pintaTodo()
         {
                var canvas = document.getElementById("thecanvas");
                var ctx = canvas.getContext("2d");
               ctx.fillStyle = "#000000";
                ctx.font = "65px fuentedurango";

//alert("no cambio cinturon");
cinturon=parseInt(<?php echo $cinturon ?>);
document.getElementById("theimage").src="../images/Fondo"+cinturon+"<?php if ($nombrecolegio=="club") echo '-club' ?>.jpg";
               var img=document.getElementById("theimage");
 
              ctx.drawImage(img,0,0,600,850);
if( parseInt(<?php echo $puntostotales ?>) < 10  )
{
    if( colegio == "club" )
    {
        ctx.fillText(parseInt(<?php echo $puntostotales ?>), 220, 520);
    }else
    {
    	ctx.fillText(parseInt(<?php echo $puntostotales ?>), 450, 465);
    }
            

}
else if( parseInt(<?php echo $puntostotales ?>) < 99 )
{
    if( colegio == "club" )
    {
        ctx.fillText(parseInt(<?php echo $puntostotales ?>), 210, 520);
    }else
    {
    	ctx.fillText(parseInt(<?php echo $puntostotales ?>), 440, 465);
    }
}
else
{
    if( colegio == "club" )
    {
        ctx.fillText(parseInt(<?php echo $puntostotales ?>), 195, 520);
    }else
    {
    	ctx.fillText(parseInt(<?php echo $puntostotales ?>), 425, 465);
    }
}


                ctx.fillStyle = "#000000";
                ctx.font = "30px fuentedurango";
                //alert("<?php echo $fechaseguimiento ?>");
                //alert("<?php echo $fechaseguimiento ?>".slice(0,4));
                //alert("<?php echo $fechaseguimiento ?>".slice(5,7));
                //alert("<?php echo $fechaseguimiento ?>".slice(8,10));
                var d = new Date("<?php echo $fechaseguimiento ?>".slice(0,4),"<?php echo $fechaseguimiento ?>".slice(5,7),"<?php echo $fechaseguimiento ?>".slice(8,10));
                //alert( d );
                //var mes = d.getMonth()+1;
                //alert( d.getDate()+"/"+mes+"/"+d.getFullYear() );
                ctx.fillText(d.getDate()+"/"+d.getMonth()+"/"+d.getFullYear(), 50, 92);
                ctx.font = "30px fuentedurango";
                ctx.fillText("<?php echo $nombrealumno ?>", 50, 180);
                //ctx.font = "20px fuentedurango";
                //ctx.fillText(idobservaciones.value, 50, 735);
                <?php
                $observaciones = json_encode($observaciones);
                echo "var out={$observaciones};";
                ?>
                if( out.length>0 )
                {
                pintaObservaciones(out);
                }
                ctx.fillStyle = "#000000";
                ctx.font = "65px fuentedurango";



if( parseInt(<?php echo $puntosextra ?>) == 10  )
{
                ctx.fillStyle = "#FFFFFF";
                ctx.font = "25px fuentedurango";
                ctx.fillText(parseInt(<?php echo $puntosextra ?>), 487, 682);
}
else if ( parseInt(<?php echo $puntosextra ?>) > 0  )
{
                ctx.fillStyle = "#FFFFFF";
                ctx.font = "25px fuentedurango";
                ctx.fillText(parseInt(<?php echo $puntosextra ?>), 492, 682);
}
                ctx.fillStyle = "#000000";
                ctx.font = "35px fuentedurango";
                ctx.fillText(parseInt(<?php echo $puntosdia ?>), 455, 608);

             var imguno=document.getElementById("iduno");
             var imgdos=document.getElementById("iddos");
             var imgtres=document.getElementById("idtres");
             var imgcuatro=document.getElementById("idcuatro");
             var imgcinco=document.getElementById("idcinco");



pintaEstrellas(1,<?php echo $actitud ?>);
pintaEstrellas(2,<?php echo $comportamiento ?>);
pintaEstrellas(3,<?php echo $participacion ?>);
pintaEstrellas(4,<?php echo $esfuerzo ?>);

pintaPilas(<?php echo $energia ?>);

}

function limpia()
           {
               pintaFondo();
            }

            function to_image()
             {
                //alert("prueba0");
                var canvas = document.getElementById("thecanvas");
                document.getElementById("theimage").src = canvas.toDataURL();
                //Canvas2Image.saveAsPNG(canvas);
                //alert("prueba");
                window.open(location.href=theimage.src);
                window.open("", "_self");
window.open(theimage.src, "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, top=500, left=500, width=300, height=700");
            }
        </script>
</head>

<body onload="pintaTodo()">
<div style="font-family: fuentedurango;">.</div>
    <div id="wrapper">


<?php include 'navegacion.php';?>
    
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Seguimiento</h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
  
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Seguimiento
                            <div class="pull-left">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Acciones
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li><a href="#" onclick="edita()">Editar</a>
                                        </li>
                                        <li><a href="#" onclick="elimina()">Eliminar</a>
                                        </li>
                                          <li><a href="crear_colegio.php" >Nuevo</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>


                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                            
                            <div class="col-sm-2">
<form action="" method="get">

  <input type="text" id="idpuntuacion" min="1" max="5" hidden>
  <input type="text" id="idcinturon" min="1" max="5" hidden>

    
<div class="botonestooltip">

<button type="button" class="btn btn-danger btn-circle btn-xl" data-toggle="tooltip" data-placement="top" title="" data-original-title="Enviar Seguimiento" onClick="enviaSeguimiento()"><i class="fa fa-envelope"></i>
                            </button>
</div>
</form>



                        
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.col-lg-2 (nested) -->
                                <div class="col-sm-10">

       <center> <canvas width=600 height=850 id="thecanvas"></canvas></center>
      <img id="theimage" width="600" height="850" src="../images/Fondo<?php echo $cinturon ?><?php if ($nombrecolegio=="club") echo '-club' ?>.jpg" alt="seguimiento" hidden>
      <img id="iduno" width="44" height="44" src="../images/estrella1.jpg" alt="uno" hidden>
      <img id="iddos" width="44" height="44" src="../images/estrella2.jpg" alt="dos" hidden>
      <img id="idtres" width="44" height="44" src="../images/estrella3.jpg" alt="tres" hidden>
      <img id="idcuatro" width="44" height="44" src="../images/estrella4.jpg" alt="cuatro" hidden>
      <img id="idcinco" width="44" height="44" src="../images/estrella5.jpg" alt="cinco" hidden>
      <img id="idbateria" width="44" height="44" src="../images/Pila.jpg" alt="cinco" hidden>
</div>




                               
                            </div>
                            <!-- /.row -->

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                 
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-8 -->
                
                    <div class="panel panel-default">
                      
                        <!-- /.panel-body -->
                    </div>
                   
                        
                </div>
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

 <script>
    // tooltip demo
    $('.botonestooltip').tooltip({
        selector: "[data-toggle=tooltip]",
        container: "body"
    })

    // popover demo
    $("[data-toggle=popover]")
        .popover()
    </script>

</body>

</html>