 <?php

$idcolegios="";
$nombrecolegios="";
$idclases="";
$nombreclases="";
//echo $uid;

$sql = "SELECT * FROM ssa_colegios";

$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) > 0) {
	// output data of each row
$i=0;
	while($row = mysqli_fetch_assoc($result)) {
		$idcolegios[$i]=$row["idcolegio"];
		$nombrecolegios[$i]=$row["nombrecolegio"];

		//echo "<script> console.log('$nombrecolegios[$i]') </script>";

		$i++;
		}
		
		} else {
			// echo "0 results";
		}
		
		$i=0;		
		$sql = "SELECT * FROM ssa_clases";
		$result = mysqli_query($conn, $sql);
		if (mysqli_num_rows($result) > 0) {
			// output data of each row
		
			while($row = mysqli_fetch_assoc($result)) {
				$idclases[$i]=$row["idclase"];
				$nombreclases[$i]=$row["nombreclase"];
				$i++;
			}
		
		} else {
			// echo "0 results";
		}
		
		
		$sql = "SELECT ssa_clases.idclase, ssa_clases.nombreclase, ssa_clases.colegio, ssa_colegios.nombrecolegio FROM ssa_clases LEFT JOIN ssa_colegios ON ssa_clases.colegio=ssa_colegios.idcolegio ORDER BY ssa_clases.colegio";
		
		$result = mysqli_query($conn, $sql);
		if (mysqli_num_rows($result) > 0) {
			// output data of each row
			$i=0;

			while($row = mysqli_fetch_assoc($result)) {
		
				$idclase=$row["idclase"];
				$nombreclase=$row["nombreclase"];
				$idcolegio=$row["colegio"];
				$nombrecolegio=$row["nombrecolegio"];
				//echo "console.log('$idclase'); ";
		
				$ids[$i]=$idcolegio."/".$idclase;
				$nombres[$i]=$nombrecolegio."/".$nombreclase;

				$i++;
			}

		
		} else {
			echo "<script> console.log('0 resultado en el join'); </script>";
		}
		
		
//mysqli_close($conn);
?>
  
  
   <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="panel_control.php">Sistema de Seguimiento Avanzado (SSA)</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
<li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
        
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#" onClick="enviaPassword()"><i class="fa fa-user fa-fw"></i> Enviar password</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i>Salir</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>                
       
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Buscar...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>

<!--
                        <li>
                            <a href="panel_control.php"><i class="fa fa-dashboard fa-fw"></i> Panel de Control</a>
                        </li>
                        <li>
                            <a href="alumnos.php"><i class="fa fa-bar-chart-o fa-fw"></i> Alumnos</span></a>
                                                     
                        </li>
                      
-->
<li>
                         
                        <li>
                            <a href="#"><i class="fa fa-sitemap fa-fw"></i> Seguimientos</span></a>
                               <!-- /.nav-second-level -->
                                <ul class="nav nav-second-level">
<?php
$arrlength = count($ids);
for($x = 0; $x < $arrlength; $x++) {
	echo "<li>";
    echo "<a  href='seguimientos-coleclase.php?coleclase=".$ids[$x]."'>".$nombres[$x]."</a>";
    echo "</li>";


}
?>
 
                            </ul>
                        </li>
                         <li>
                            <a href="#"><i class="fa fa-sitemap fa-fw"></i> Alumnos</span></a>
                               <!-- /.nav-second-level -->
                                                               <ul class="nav nav-second-level">
                                                               
                                                               <?php
$arrlength = count($ids);
for($x = 0; $x < $arrlength; $x++) {
	echo "<li>";
    echo "<a  href='alumnos-coleclase.php?coleclase=".$ids[$x]."'>".$nombres[$x]."</a>";
    echo "</li>";


}
?>
                            </ul>
                        </li>
                                                 <li>
                            <a href="colegios.php"><i class="fa fa-sitemap fa-fw"></i> Colegios</span></a>
                               <!-- /.nav-second-level -->
                        </li>
                                            <li>
                            <a href="entrenadores.php"><i class="fa fa-sitemap fa-fw"></i> Entrenadores</span></a>
                               <!-- /.nav-second-level -->
                        </li>

                       
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
 