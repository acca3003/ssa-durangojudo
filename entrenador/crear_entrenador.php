<!DOCTYPE html>
<html lang="es">

<head>

<title> Entrenador  </title>
<?php include 'cabecera.php';?>
<?php include 'seguridad.php';?>


<script>


function crearColegio() {

	//console.log("insertando: " + document.getElementById("idnombrecolegio").value + "/"+ document.getElementById("iddireccioncolegio").value);


	$.ajax({
	  type: "POST",
	  url: "inserta_entrenador.php",
	  data: 
	{ 

		     nombreusuario: document.getElementById("idnombreusuario").value,
		     passwordusuario: document.getElementById('idpasswordusuario').value,
		     rolusuario: document.getElementById('idrolusuario').value,

	  }
	}).done(function(o) {
	  console.log('creado'); 
	  console.log(o);
	  document.getElementById("textoventana").innerHTML=o;
	  document.getElementById("ventana").click();
	  // If you want the file to be visible in the browser 
	  // - please modify the callback in javascript. All you
	  // need is to return the url to the file, you just saved 
	  // and than put the image in your browser.
	});
		
}


</script>
</head>

<body>

<?php include 'navegacion.php';?>
    
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Nuevo Entrenador</h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
  
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Nuevo colegio                           
                        </div>


                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="row">
 				<div id="divCheckbox" style="display: none;">
					<button id="ventana" class="btn btn-primary btn-lg"
						data-toggle="modal" data-target="#myModal" hidden="hidden">
						Ventana con mensajes</button>
				</div>
                            
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog"
								aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal"
												aria-hidden="true">&times;</button>
											<h4 class="modal-title" id="myModalLabel">Aviso!</h4>
										</div>
										<div class="modal-body" id="textoventana"></div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default"
												data-dismiss="modal">Cerrar</button>
										</div>
									</div>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>
							
							
                            <div class="col-sm-2">
<form action="" method="get" class="form-inline" >
			<div class="form-group">
									<label for="idnombreusuario">Nombre (direccion de correo):</label>
									<input type="email" id="idnombreusuario" class="form-control" required placeholder="Introduce tu email"> <br>
									<label for="idpasswordusuario">Password:</label>
									<input type="text" id="idpasswordusuario" class="form-control" required placeholder="Introduce tu password para SSA> <br>
									<label for="idrolusuario">Rol(no modificar):</label>
									<input type="text" id="idrolusuario" class="form-control" value="entrenador" disabled><br>
			</div>
    
<div class="botonestooltip">

									<button type="button" class="btn btn-success btn-circle btn-xl"
										data-toggle="tooltip" data-placement="top" title=""
										data-original-title="Crear Colegio"
										onClick="crearColegio()">
										<i class="fa fa-database"></i>
									</button>
									 <!-- <button type="button" class="btn btn-warning btn-circle btn-xl"
										data-toggle="tooltip" data-placement="top" title=""
										data-original-title="Borrar Seguimiento" onClick="limpia()">
										<i class="fa fa-eraser"></i>
									</button> -->
									
</div>
</form>



                        
                                </div>
                                <!-- /.col-lg-2 (nested) -->
                                                              
                            </div>
                            <!-- /.row -->

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                 
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-8 -->
                
                    <div class="panel panel-default">
                      
                        <!-- /.panel-body -->
                    </div>
                   
                        
                </div>
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    
      <?php include 'pie.php';?>  

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

 <script>
    // tooltip demo
    $('.botonestooltip').tooltip({
        selector: "[data-toggle=tooltip]",
        container: "body"
    })

    // popover demo
    $("[data-toggle=popover]")
        .popover()
    </script>

</body>

