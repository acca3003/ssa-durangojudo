<!DOCTYPE html>
<html lang="en">

<head>

<?php include 'cabecera.php';?>
<title> Listado de Alumnos </title>
</head>

<body>

    <div id="wrapper">


<?php include 'navegacion.php';?>
    
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Listado de Alumnos</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
  
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Listado de Alumnos
                            <div class="pull-right">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Acciones
                                        <span class="caret"></span>
                                    </button>
<script>
function nuevoSeguimiento ()
{
  $('#listadocolegios').find('tr').each(function() {
    var row = $(this);
    if (row.find('input[type="checkbox"]').is(':checked')) {
     window.location = "crear_seguimiento.php?alumno=" + encodeURIComponent(row.find("td:nth-child(2)").text())
    // alert(row.find("td:nth-child(2)").text());
    // alert(row.find("td:nth-child(3)").text());
    } 
   })
}
function edita ()
{
  $('#listadocolegios').find('tr').each(function() {
    var row = $(this);
    if (row.find('input[type="checkbox"]').is(':checked')) {
    //alert(row.find("td:nth-child(2)").text());
     //alert(row.find("td:nth-child(3)").text());
    } 
   })
}
function elimina ()
{
  $('#listadocolegios').find('tr').each(function() {
    var row = $(this);
    if (row.find('input[type="checkbox"]').is(':checked')) {
    // alert(row.find("td:nth-child(2)").text());
    //alert(row.find("td:nth-child(3)").text());
    } 
   })
}

</script>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li><a href="#" onclick="nuevoSeguimiento()">Nuevo Seguimiento</a>
                                        </li>
                                        <li><a href="#" onclick="elimina()">Eliminar</a>
                                        </li>
                                          <li><a href="crear_colegio.php" >Nuevo</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">

                                    <div class="table-responsive">
                                              <table class="table table-bordered table-hover table-striped" id="listadocolegios">
                                            <thead>
                                                <tr>
                                                    <th style="width:20px;"><input type="checkbox"></th>
                                                    <th>#</th>
                                                    <th>Colegio</th>
                                                    <th>Clase</th>
                                                    <th>Nombre</th>
                                                    <th>Apellidos</th>
                                                </tr>
                                            </thead>


                                            <tbody>

<?php
$conn = mysqli_connect($servername, $username, $password, $dbname,"3306");
// Check connection
if (!$conn) {
	die("Connection failed: " . mysqli_connect_error());
}
$sql = "SELECT * FROM ssa_alumnos";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    // output data of each row
    
    while($row = mysqli_fetch_assoc($result)) {
        echo "<tr>";
        echo "<td><input type='checkbox'></td><td id=idalumno> " . $row["idalumno"]. "</td><td id=idcolegio> " . $row["colegio"]. "</td><td id=idclase> " . $row["clase"]. "</td><td id=idnombrealumno>" . $row["nombrealumno"]. "</td><td id=idapellidosalumno> " . $row["apellidosalumno"]. "</td>";
        echo "</tr>";
    }
} else {
    //echo "0 results";
}

mysqli_close($conn);
?>  

                                            </tbody>
                                        </table>

<script>$('table').tablecheckbox();</script>


                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.col-lg-4 (nested) -->
                               
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                 
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-8 -->
                
                    <div class="panel panel-default">
                      
                        <!-- /.panel-body -->
                    </div>
                   
                        
                </div>
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>