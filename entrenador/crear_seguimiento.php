<!DOCTYPE html>
<html lang="en">

<head>
<style>
@font-face {
	font-family: fuentedurango;
	src: url("/fonts/VAGRoundedBold.ttf");
}
</style>
<title>Seguimiento</title>
<?php include 'cabecera.php';?>
<?php include 'seguridad.php';?>

<?php

$sql = "SELECT * FROM ssa_alumnos where idalumno=" . $_GET ['alumno'];
$uid = $_GET ['alumno'];
// echo $uid;

$result = mysqli_query ( $conn, $sql );

// echo $sql;

if (mysqli_num_rows ( $result ) > 0) {
	// output data of each row
	
	while ( $row = mysqli_fetch_assoc ( $result ) ) {
		$alumno = $row ["nombrealumno"];
		$apellidos = $row ["apellidosalumno"];
		$cinturon = $row ["cinturon"];
		$puntuacion = $row ["puntuacion"];
		$colegio =  $row ["colegio"];
	}
} else {
	// echo "0 results";
}

$sql = "SELECT * FROM ssa_colegios where idcolegio=$colegio";
$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) > 0) {
	// output data of each row
	while($row = mysqli_fetch_assoc($result)) {
			$nombrecolegio2 = $row["nombrecolegio"];
	}
} else {
	//echo "0 results";
}
//mysqli_close($conn);
?>  
 <script>

var d = new Date();
var cargado = "no";
var puntostotales=0;
var puntosdia=0;
var puntosextra=0;
var cinturon=0;
var colegio="<?php echo $nombrecolegio2 ?>";
alert( colegio );
function ratonCargaFondo(e) {
if( cargado == "no" )
{
       pintaFondo();
       cargado="si";
}

}
function pintaObservaciones(observaciones) {
    var res = observaciones.split(" ");
    var texto ="";
    var texto1 = "";
    var ndemo =1;
    var canvas = document.getElementById("thecanvas");
    var ctx = canvas.getContext("2d");
    var indicelineas=0;

    // recorremos todo el array y vamos creando un string hasta tener el maximo de caracteres
    for	(index = 0; index < res.length; index++) {
         //alert( "indice " + index + "res " + res[index]);
          texto1 = texto + " " + res[index];
          if( texto1.length > 55 )
          {
               //imprimir texto y ponerlo con el actual de inicio
               //alert( texto );
                
                ctx.font = "20px fuentedurango";
                ctx.fillText(texto, 50, 735+indicelineas*20);
               texto = " " + res[index];
               indicelineas++;

          }
          else
          {
               texto = texto1;
          }
    }
                ctx.font = "20px fuentedurango";
                ctx.fillText(texto, 50, 735+indicelineas*20);

   // document.getElementById("demo").innerHTML = res;
}
function cargaInicial(){
    cargaAlumno();
    pintaFondo();
//alert("cargando ficha");
    limpia();
}
function enviaSeguimiento() {
pintaTodo();


$.ajax({
  type: "POST",
  url: "envia_seguimiento.php",
  data: { 
     imgBase64: document.getElementById("thecanvas").toDataURL("image/jpg",0.7),
     uid: <?php echo $uid ?>,
     fechaseguimiento: document.getElementById('idfecha').value,
    	     actitud: document.getElementById("idactitud").value,
    	     comportamiento: document.getElementById('idcomportamiento').value,
    	     esfuerzo: document.getElementById('idesfuerzo').value,
    	     energia: document.getElementById('idenergia').value,
    	     participacion: document.getElementById('idparticipacion').value,
    	     observaciones: document.getElementById('idobservaciones').value,
    	     puntosextra: puntosextra,
    	     puntosdia: puntosdia,
    	     puntostotales: puntostotales,
    	     cinturon: cinturon
  }
}).done(function(o) {
   console.log('creado'); 
  console.log(o);
  document.getElementById("textoventana").innerHTML=o;
  document.getElementById("ventana").click();
  // If you want the file to be visible in the browser 
  // - please modify the callback in javascript. All you
  // need is to return the url to the file, you just saved 
  // and than put the image in your browser.
});
}

function guardaSeguimiento(){
pintaTodo();

                       //alert("puntos dia: " + puntosdia + "puntosextras: " + puntosextra);

//alert (cinturon);
   
$.ajax({
  type: "POST",
  url: "guarda_seguimiento.php",
  data: 
{ 
     uid: <?php echo $uid ?>,
     actitud: document.getElementById("idactitud").value,
     comportamiento: document.getElementById('idcomportamiento').value,
     esfuerzo: document.getElementById('idesfuerzo').value,
     energia: document.getElementById('idenergia').value,
     fechaseguimiento: document.getElementById('idfecha').value,
     participacion: document.getElementById('idparticipacion').value,
     observaciones: document.getElementById('idobservaciones').value,
     puntosextra: puntosextra,
     puntosdia: puntosdia,
     puntostotales: puntostotales,
     cinturon: cinturon
  }
}).done(function(o) {
  console.log('creado'); 
  console.log(o);
  document.getElementById("textoventana").innerHTML=o;
  document.getElementById("ventana").click();
  // If you want the file to be visible in the browser 
  // - please modify the callback in javascript. All you
  // need is to return the url to the file, you just saved 
  // and than put the image in your browser.
});

}



function cargaAlumno()
{
<?php

echo "document.getElementById('idnombre').value='", $alumno, " ", $apellidos, "';";
// echo "document.getElementById('idcinturon').value='", $cinturon , "';";
echo "document.getElementById('idpuntuacion').value='", $puntuacion, "';";

?>  
}

 function updateTextInput(val) {
      document.getElementById('textInput').value=val; 
    }

function pintaFondo()
{

                var canvas = document.getElementById("thecanvas");
                var ctx = canvas.getContext("2d");
                if ( colegio.toLowerCase()=="club")
                {
                	alert( "es de club antes");
                	document.getElementById("theimage").src="../images/Fondo<?php echo $cinturon ?>-club.jpg";
                	alert( "es de club despues");
                }
                else
                {
                    document.getElementById("theimage").src="../images/Fondo<?php echo $cinturon ?>.jpg";
                }
             
               var img=document.getElementById("theimage");
               ctx.drawImage(img,0,0,600,850);
                ctx.fillStyle = "#000000";
                ctx.font = "65px fuentedurango";
                
                if( (parseInt(document.getElementById('idpuntuacion').value)) < 10   )
                {
                    if( colegio == "club" )
                    {
                                ctx.fillText(puntostotales, 220, 520);
                    }else
                    {
                        ctx.fillText(puntostotales, 450, 465);
                    }
                }
                else if( (parseInt(document.getElementById('idpuntuacion').value)) < 99  )
                {
                	
                    if( colegio == "club" )
                    {
                                ctx.fillText(puntostotales, 210, 520);
                    }else
                    {
                        ctx.fillText(puntostotales, 440, 465);
                    }
                }
                else
                {

                    if( colegio == "club" )
                    {
                        ctx.fillText(puntostotales, 195, 520);
                    }else
                    {
                        ctx.fillText(puntostotales, 425, 465);
                    }
                }
}

function vacia()
{

                var canvas = document.getElementById("thecanvas");
                var ctx = canvas.getContext("2d");

if( (parseInt(document.getElementById('idpuntuacion').value)+parseInt(document.getElementById('idpuntos').value)+parseInt(document.getElementById('idextrapuntos').value)) >= 200  )
{

//alert("cambio cinturon");
cinturon=parseInt(<?php echo $cinturon ?>)+1;

if ( colegio.toLowerCase()=="club")
{
	document.getElementById("theimage").src="../images/Fondo"+cinturon+"-club.jpg";
}
else
{
	document.getElementById("theimage").src="../images/Fondo"+cinturon+".jpg";
}
               var img=document.getElementById("theimage");
 
              ctx.drawImage(img,0,0,600,850);


}
else
{

                if ( colegio.toLowerCase()=="club")
                {
                	document.getElementById("theimage").src="../images/Fondo<?php echo $cinturon ?>-club.jpg";
                }
                else
                {
                	document.getElementById("theimage").src="../images/Fondo<?php echo $cinturon ?>.jpg";
                }
               var img=document.getElementById("theimage");
               ctx.drawImage(img,0,0,600,850);

}
}

function pintaEstrellas(n, posicion)
{

             var estrella1=document.getElementById("iduno");
             var estrella2=document.getElementById("iddos");
             var estrella3=document.getElementById("idtres");
             var estrella4=document.getElementById("idcuatro");
             var estrella5=document.getElementById("idcinco");
             var estrellas = [estrella1, estrella2, estrella3, estrella4, estrella5];
         var canvas = document.getElementById("thecanvas");
                var ctx = canvas.getContext("2d");
//alert("valor de n: " + n + " posicion: "+posicion);
                if ( colegio.toLowerCase()=="club")
                {

                }
                else
                {
             for (i = 0; i < posicion; i++) { 
                       ctx.drawImage(estrellas[i],50+i*50,238+(n-1)*84,32,30);
//alert("buclue i: " + i);
              }
                }

}

function pintaPilas(posicion)
{
        var canvas = document.getElementById("thecanvas");
                var ctx = canvas.getContext("2d");
             var pila=document.getElementById("idbateria");

             if ( colegio.toLowerCase()=="club")
             {

             }
             else
             {
              for (i = 0; i < posicion; i++) { 
                       ctx.drawImage(pila,50+i*50,582,32,30);
//alert("buclue i: " + i);
              }
             }
}

function pintaTodo()
         {

          vacia();

                var canvas = document.getElementById("thecanvas");
                var ctx = canvas.getContext("2d");
               ctx.fillStyle = "#000000";
                ctx.font = "65px fuentedurango";
                puntostotales=0;
                //alert(isNaN(document.getElementById('idextrapuntos2').value));
                //alert(isNaN(document.getElementById('idpuntos2').value));
               if( (document.getElementById('idextrapuntos2').value != "") && (document.getElementById('idpuntos2').value != "") && (document.getElementById('idpuntostotales2').value != "") ) 
               {
               
                     if( (parseInt(document.getElementById('idpuntos2').value) > 0) || (parseInt(document.getElementById('idextrapuntos2').value) > 0) || (parseInt(document.getElementById('idpuntostotales2').value) > 0))
                      {
                          if ( parseInt(document.getElementById('idpuntostotales2').value) > 0 )
                        	{
                          
                              puntostotales = parseInt(document.getElementById('idpuntostotales2').value);
                              puntosdia = 0;
                              puntosextra = 0;  
                            	
                        	}
                          else
                          {
                            
                              puntostotales = parseInt(document.getElementById('idpuntuacion').value) + parseInt(document.getElementById('idpuntos2').value) + parseInt(document.getElementById('idextrapuntos2').value);
                              puntosdia = parseInt(document.getElementById('idpuntos2').value);
                              puntosextra = parseInt(document.getElementById('idextrapuntos2').value);
                          }
                		  

                //alert ("hay puntos manuales" + puntostotales  );
                      }
                      else
                      {
                        
                               puntostotales = parseInt(document.getElementById('idpuntuacion').value) + parseInt(document.getElementById('idpuntos').value) + parseInt(document.getElementById('idextrapuntos').value);
                               puntosdia = parseInt(document.getElementById('idpuntos').value);
                               puntosextra = parseInt(document.getElementById('idextrapuntos').value);
                
                      }
                
                }else
                {
                  
                               puntostotales = parseInt(document.getElementById('idpuntuacion').value) + parseInt(document.getElementById('idpuntos').value) + parseInt(document.getElementById('idextrapuntos').value);
                               puntosdia = parseInt(document.getElementById('idpuntos').value);
                               puntosextra = parseInt(document.getElementById('idextrapuntos').value);
                 
                
                
                }
                       

                               
if( puntostotales >= 200  )
{

//alert("cambio cinturon pintatodo");
//cinturon=parseInt(<?php echo $cinturon ?>)+1;
//alert( "cinturon nuevo " + cinturon);
//document.getElementById("theimage").src="../images/Fondo"+cinturon+".jpg";
//alert( "nuevo fondo " + "../images/Fondo"+cinturon+".jpg");
               var img=document.getElementById("theimage2");
 
              ctx.drawImage(img,0,0,600,850);
              if( colegio == "club" )
              {
                  ctx.fillText(puntostotales-200, 200, 520);
              }else
              {
              	ctx.fillText(puntostotales-200, 450, 465);
              }
             puntostotales=puntostotales-200;
}
else
{
//alert("no cambio cinturon");
cinturon=parseInt(<?php echo $cinturon ?>);
//document.getElementById("theimage").src="../images/Fondo"+cinturon+".jpg";
               var img=document.getElementById("theimage");
 
              ctx.drawImage(img,0,0,600,850);
if( puntostotales < 10  )
{
    if( colegio == "club" )
    {
                ctx.fillText(puntostotales, 220, 520);
    }else
    {
        ctx.fillText(puntostotales, 450, 465);
    }
}
else if( puntostotales < 99 )
{
	
    if( colegio == "club" )
    {
                ctx.fillText(puntostotales, 210, 520);
    }else
    {
        ctx.fillText(puntostotales, 440, 465);
    }
}
else
{

    if( colegio == "club" )
    {
                ctx.fillText(puntostotales, 195, 520);
    }else
    {
        ctx.fillText(puntostotales, 425, 465);
    }
}


}

                ctx.fillStyle = "#000000";
                ctx.font = "30px fuentedurango";
                var d = new Date(document.getElementById('idfecha').value);
                var mes = d.getMonth()+1;
                ctx.fillText(d.getDate()+"/"+mes+"/"+d.getFullYear(), 50, 92);
                ctx.font = "30px fuentedurango";
                ctx.fillText(idnombre.value, 50, 180);
                //ctx.font = "20px fuentedurango";
                //ctx.fillText(idobservaciones.value, 50, 735);
                pintaObservaciones(idobservaciones.value);
                ctx.fillStyle = "#000000";
                ctx.font = "65px fuentedurango";



if( puntosextra >= 10  )
{
                ctx.fillStyle = "#FFFFFF";
                ctx.font = "25px fuentedurango";
                ctx.fillText(puntosextra, 487, 682);
}
else if ( puntosextra > 0  )
{
                ctx.fillStyle = "#FFFFFF";
                ctx.font = "25px fuentedurango";
                ctx.fillText(puntosextra, 492, 682);
}
                ctx.fillStyle = "#000000";
                ctx.font = "35px fuentedurango";
                ctx.fillText(puntosdia, 455, 608);

             var imguno=document.getElementById("iduno");
             var imgdos=document.getElementById("iddos");
             var imgtres=document.getElementById("idtres");
             var imgcuatro=document.getElementById("idcuatro");
             var imgcinco=document.getElementById("idcinco");



pintaEstrellas(1,document.getElementById("idactitud").value);
pintaEstrellas(2,document.getElementById("idcomportamiento").value);
pintaEstrellas(3,document.getElementById("idparticipacion").value);
pintaEstrellas(4,document.getElementById("idesfuerzo").value);

pintaPilas(document.getElementById("idenergia").value);

}

function limpia()
           {
               pintaFondo();
            }

            function to_image()
             {
                //alert("prueba0");
                var canvas = document.getElementById("thecanvas");
                document.getElementById("theimage").src = canvas.toDataURL();
                //Canvas2Image.saveAsPNG(canvas);
                //alert("prueba");
                window.open(location.href=theimage.src);
                window.open("", "_self");
window.open(theimage.src, "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, top=500, left=500, width=300, height=700");
            }
        </script>
</head>

<body onload="cargaInicial()">
	<div style="font-family: fuentedurango;">.</div>
	<div id="wrapper">
             

<?php include 'navegacion.php';?>
    
        <div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h3 class="page-header">Seguimiento</h3>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->

			<div class="panel panel-default">
				<div class="panel-heading">
					<i class="fa fa-bar-chart-o fa-fw"></i> Seguimiento
					<div class="pull-left">
						<div class="btn-group">
							<button type="button"
								class="btn btn-default btn-xs dropdown-toggle"
								data-toggle="dropdown">
								Acciones <span class="caret"></span>
							</button>
							<script>



</script>
							<ul class="dropdown-menu pull-right" role="menu">
								<!--  <li><a href="#">Editar</a></li> -->
								<!-- <li><a href="elimina_seguimiento.php">Eliminar seguimiento</a></li> -->
								<li><a href="crear_seguimiento.php?alumno=<?php echo $_GET ['alumno'] ?>">Nuevo Seguimiento</a></li>
							</ul>
						</div>
					</div>
				</div>



				<div id="divCheckbox" style="display: none;">
					<button id="ventana" class="btn btn-primary btn-lg"
						data-toggle="modal" data-target="#myModal" hidden="hidden">
						Ventana con mensajes</button>
				</div>

				<!-- /.panel-heading -->
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-8">
							<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
								aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal"
												aria-hidden="true">&times;</button>
											<h4 class="modal-title" id="myModalLabel">Aviso!</h4>
										</div>
										<div class="modal-body" id="textoventana"></div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default"
												data-dismiss="modal">Cerrar</button>
										</div>
									</div>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>



							<center>
								<canvas width=600 height=850 id="thecanvas"></canvas>
							</center>
							<img id="theimage" width="600" height="850" borra=<?php echo $nombrecolegio2 ?> 
								src="../images/Fondo<?php echo $cinturon ?><?php if (strcasecmp($nombrecolegio2,"club")) echo '-club' ?>.jpg"
								alt="seguimiento<?php echo $nombrecolegio2 ?>" hidden>
								
							<img id="theimage2" width="600"
								height="850" src="../images/Fondo<?php echo $cinturon+1 ?><?php if (strcasecmp($nombrecolegio2,"club")) echo '-club' ?>.jpg"
								alt="seguimiento2<?php echo $nombrecolegio2 ?>" hidden> 
								
							<img id="iduno" width="44"
								height="44" src="../images/estrella1.jpg" alt="uno" hidden>
							<img
								id="iddos" width="44" height="44" src="../images/estrella2.jpg"
								alt="dos" hidden>
							<img id="idtres" width="44" height="44"
								src="../images/estrella3.jpg" alt="tres" hidden> <img
								id="idcuatro" width="44" height="44"
								src="../images/estrella4.jpg" alt="cuatro" hidden> <img
								id="idcinco" width="44" height="44"
								src="../images/estrella5.jpg" alt="cinco" hidden> <img
								id="idbateria" width="44" height="44" src="../images/Pila.jpg"
								alt="cinco" hidden>
						</div>



						<div class="col-sm-4">
							<form action="" method="get">
								Nombre y Apellidos: <input type="text" id="idnombre" value=""
									size=50"><br> Fecha del Seguimiento: <input type="date"
									id="idfecha" value=<?php echo date("Y-m-d") ?>><br>

								<div class="table-responsive">
									<table class="table table-bordered table-striped">

										<tbody>
											<tr>
												<td colspan="5">Actitud:</td>
												<td colspan="5">Comportamiento:</td>
											</tr>
											<tr>
												<th><center>1</center></th>
												<th><center>2</center></th>
												<th><center>3</center></th>
												<th><center>4</center></th>
												<th><center>5</center></th>
												<th><center>1</center></th>
												<th><center>2</center></th>
												<th><center>3</center></th>
												<th><center>4</center></th>
												<th><center>5</center></th>
											</tr>

											<td colspan="5"><input type="range" id="idactitud" min="1"
												max="5"></td>
											<td colspan="5"><input type="range" id="idcomportamiento"
												min="1" max="5"></td>
											</tr>

											<tr>
												<td colspan="5">Participacion:</td>
												<td colspan="5">Esfuerzo:</td>
											</tr>
											<tr>
												<th><center>1</center></th>
												<th><center>2</center></th>
												<th><center>3</center></th>
												<th><center>4</center></th>
												<th><center>5</center></th>
												<th><center>1</center></th>
												<th><center>2</center></th>
												<th><center>3</center></th>
												<th><center>4</center></th>
												<th><center>5</center></th>

											</tr>
											<tr>
												<td colspan="5"><input type="range" id="idparticipacion"
													min="1" max="5"></td>
												<td colspan="5"><input type="range" id="idesfuerzo" min="1"
													max="5"></td>
											</tr>
											<tr>
												<td colspan="5">Energia:</td>
											</tr>
											<tr>
												<th><center>1</center></th>
												<th><center>2</center></th>
												<th><center>3</center></th>
												<th><center>4</center></th>
												<th><center>5</center>
											
											
											<tr>
												<td colspan="5"><input type="range" id="idenergia" min="1"
													max="5"></td>
											</tr>
											<tr>
												<td colspan="5">Puntos:<input type="text" id="idpuntos2"
													value="0" size="2"></td>
												<td colspan="5">Totales:<input type="text"
													id="idpuntostotales2" value="0" size="3"></td>
											</tr>
											<tr>
												<th><center>0</center></th>
												<th><center>1</center></th>
												<th><center>2</center></th>
												<th><center>3</center></th>
												<th><center>4</center></th>
												<th><center>5</center></th>
												<th><center>6</center>
											
											
											<tr>
												<td colspan="7"><input type="range" id="idpuntos" min="0"
													max="6"></td>
											</tr>

											<tr>
												<td colspan="5">Extra Puntos:<input type="text"
													id="idextrapuntos2" value="0" size="2"></td>
											</tr>
											<tr>
												<th><center>0</center></th>
												<th><center>1</center></th>
												<th><center>2</center></th>
												<th><center>3</center></th>
												<th><center>4</center></th>
												<th><center>5</center></th>
												<th><center>6</center></th>
												<th><center>7</center></th>
												<th><center>8</center></th>
												<th><center>9</center></th>
												<th><center>10</center></th>
											
											
											<tr>
												<td colspan="11"><input type="range" id="idextrapuntos"
													value="0" min="0" max="10"></td>
											</tr>

										</tbody>
									</table>
								</div>



								<input type="text" id="idpuntuacion" min="1" max="5" hidden> <input
									type="text" id="idcinturon" min="1" max="5" hidden>

								<div class="form-group">
									<label for="idobservaciones">Observaciones:</label>
									<textarea class="form-control" rows="5" id="idobservaciones"></textarea>
								</div>

								<div class="botonestooltip">
									<button type="button" class="btn btn-primary btn-circle btn-xl"
										data-toggle="tooltip" data-placement="top" title=""
										data-original-title="Pintar Seguimiento" onClick="pintaTodo()">
										<p class="fa fa-pencil"></p>
									</button>

									<button type="button" class="btn btn-warning btn-circle btn-xl"
										data-toggle="tooltip" data-placement="top" title=""
										data-original-title="Borrar Seguimiento" onClick="limpia()">
										<i class="fa fa-eraser"></i>
									</button>

									<button type="button" class="btn btn-success btn-circle btn-xl"
										data-toggle="tooltip" data-placement="top" title=""
										data-original-title="Guardar Seguimiento"
										onClick="guardaSeguimiento()">
										<i class="fa fa-database"></i>
									</button>

									<button type="button" class="btn btn-danger btn-circle btn-xl"
										data-toggle="tooltip" data-placement="top" title=""
										data-original-title="Enviar Seguimiento"
										onClick="enviaSeguimiento()">
										<i class="fa fa-envelope"></i>
									</button>
								</div>
							</form>




							<!-- /.table-responsive -->
						</div>
						<!-- /.col-lg-4 (nested) -->

					</div>
					<!-- /.row -->

				</div>
				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->

			<!-- /.panel -->
		</div>
		<!-- /.col-lg-8 -->

		<div class="panel panel-default">

			<!-- /.panel-body -->
		</div>


	</div>
	<!-- /.col-lg-4 -->
	</div>
	<!-- /.row -->
	</div>
	<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->


  <?php include 'pie.php';?>  
  
  
	<!-- jQuery -->
	<script src="../bower_components/jquery/dist/jquery.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

	<!-- Custom Theme JavaScript -->
	<script src="../dist/js/sb-admin-2.js"></script>

	<script>
    // tooltip demo
    $('.botonestooltip').tooltip({
        selector: "[data-toggle=tooltip]",
        container: "body"
    })

    // popover demo
    $("[data-toggle=popover]")
        .popover()
    </script>

</body>

</html>