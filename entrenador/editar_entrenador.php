<!DOCTYPE html>
<html lang="es">

<head>

<title> Entrenador  </title>
<?php include 'cabecera.php';?>
<?php include 'seguridad.php';?>

<?php

$sql = "SELECT * FROM ssa_usuarios where idusuario=" . $_GET['uid'];
$uid=$_GET['uid'];
$nombreusuario="";
$rolusuario="";
$passwordusuario="";
//echo $uid;

$result = mysqli_query($conn, $sql);

//echo $sql;

if (mysqli_num_rows($result) > 0) {
    // output data of each row
    
    while($row = mysqli_fetch_assoc($result)) {
       $nombreusuario=$row["nombreusuario"];
       $rolusuario=$row["rolusuario"];
       $passwordusuario=$row["passwordusuario"];

  
    }
} else {
    // echo "0 results";
}

//mysqli_close($conn);
?>  
<script>

function limpia() {

}

function guardaEntrenador() {

	  
	$.ajax({
	  type: "POST",
	  url: "guarda_entrenador.php",
	  data: 
	{ 
	     usuario: <?php echo $_GET['uid'] ?>,
	     nombreusuario: document.getElementById("idnombreusuario").value,
	     passwordusuario: document.getElementById('idrolusuario').value,
	     rolusuario: document.getElementById('idpasswordusuario').value,
	  }
	}).done(function(o) {
	  console.log('creado'); 
	  console.log(o);
	  document.getElementById("textoventana").innerHTML=o;
	  document.getElementById("ventana").click();
	  // If you want the file to be visible in the browser 
	  // - please modify the callback in javascript. All you
	  // need is to return the url to the file, you just saved 
	  // and than put the image in your browser.
	});
		
}


</script>
</head>

<body>

<?php include 'navegacion.php';?>
    
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Editar Entrenador</h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
  
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> 
                           
                        </div>


                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="row">
 				<div id="divCheckbox" style="display: none;">
					<button id="ventana" class="btn btn-primary btn-lg"
						data-toggle="modal" data-target="#myModal" hidden="hidden">
						Ventana con mensajes</button>
				</div>
                            
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog"
								aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal"
												aria-hidden="true">&times;</button>
											<h4 class="modal-title" id="myModalLabel">Aviso!</h4>
										</div>
										<div class="modal-body" id="textoventana"></div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default"
												data-dismiss="modal">Cerrar</button>
										</div>
									</div>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>
							
							
                            <div class="col-sm-4">
<form action="" method="get" class="form-inline" >
			<div class="form-group">
									<label for="idnombreusuario">Nombre:</label>
									<input type="text" id="idnombreusuario" class="form-control" value="<?php echo $nombreusuario ?>" ><br>
									<label for="idpasswordusuario">Password:</label>
									<input type="text" id="idpasswordusuario" class="form-control" value="<?php echo $passwordusuario ?>" ><br>
									<label for="idrolusuario">Rol(no modificar):</label>
									<input type="text" id="idrolusuario" class="form-control" value="<?php echo $rolusuario ?>" disabled><br>
		
								</div>
    
<div class="botonestooltip">

									<button type="button" class="btn btn-success btn-circle btn-xl"
										data-toggle="tooltip" data-placement="top" title=""
										data-original-title="Guardar Entrenador"
										onClick="guardaEntrenador()">
										<i class="fa fa-database"></i>
									</button>
									 <!-- <button type="button" class="btn btn-warning btn-circle btn-xl"
										data-toggle="tooltip" data-placement="top" title=""
										data-original-title="Borrar Seguimiento" onClick="limpia()">
										<i class="fa fa-eraser"></i>
									</button> -->
									
</div>
</form>



                        
                                </div>
                                <!-- /.col-lg-2 (nested) -->
                                                              
                            </div>
                            <!-- /.row -->

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                 
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-8 -->
                
                    <div class="panel panel-default">
                      
                        <!-- /.panel-body -->
                    </div>
                   
                        
                </div>
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    
      <?php include 'pie.php';?>  
      

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

 <script>
    // tooltip demo
    $('.botonestooltip').tooltip({
        selector: "[data-toggle=tooltip]",
        container: "body"
    })

    // popover demo
    $("[data-toggle=popover]")
        .popover()
    </script>

</body>

</html>