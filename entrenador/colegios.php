<!DOCTYPE html>
<html lang="en">
<head>

<?php include 'cabecera.php';?>
<?php include 'seguridad.php';?>

<title> Colegios en SSA </title>


<link rel="stylesheet" type="text/css" href="../bower_components/datatables/media/css/jquery.dataTables.min.css">
</head>

<body>

    <div id="wrapper">

<?php include 'navegacion.php';?>

<script>

				function verClasesColegio ()
				{
 
						
						var table = $('#listadocolegios').DataTable();
						//alert(table.row('.selected').data()[0]);
						if( table.row('.selected').length > 0 )
						{
						window.location = "ver_clases.php?colegio=" + encodeURIComponent(table.row('.selected').data()[0]);
   						}
						
						
														 
				}

	
				function editaColegio ()
				{
					var table = $('#listadocolegios').DataTable();
					//alert(table.row('.selected').data()[0]);
					if( table.row('.selected').length > 0 )
					{
						window.location = "editar_colegio.php?colegio=" + encodeURIComponent(table.row('.selected').data()[0]);
						}
														 
				}
				function eliminaColegio ()
				{
					$('#listadocolegios').find('tr').each(function() {
						var row = $(this);
						if (row.find('input[type="checkbox"]').is(':checked')) {
							// alert(row.find("td:nth-child(2)").text());
							//alert(row.find("td:nth-child(3)").text());
						}
					}
														 )
				}
			
</script>

<div id="page-wrapper">
   <div class="row">
       <div class="col-lg-12">
             <h3 class="page-header">Gestion de Colegios del SSA</h3>
        </div>
                <!-- /.col-lg-12 -->
    </div>
           
    <div class="row">

                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Listado de Colegios
                            <div class="pull-left">
                                <div class="btn-group">
                                    <?php include 'acciones_colegios.php';?>
                                </div>
                            </div>
                        </div>


<?php include 'listado_colegios.php';?>



    </div>
    <!-- /.row -->

</div>
    <!-- /#wrapper -->


  <?php include 'pie.php';?>  
  
  
    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

  <script>
  /*  $(document).ready(function() {
        $('#listadoalumnos').DataTable({
                responsive: true
        });
    });*/
    

    </script>

</body>

</html>	