<!DOCTYPE html>
<html lang="es">

<head>

<title> Alumno  </title>
<?php include 'cabecera.php';?>
<?php include 'seguridad.php';?>

<?php

$sql = "SELECT * FROM ssa_alumnos where idalumno=" . $_GET['uid'];
$uid = $_GET['uid'];
$idcolegios="";
$nombrecolegios="";
$idclases="";
$nombreclases="";
//echo $uid;

$result = mysqli_query($conn, $sql);

//echo $sql;

if (mysqli_num_rows($result) > 0) {
    // output data of each row
    
    while($row = mysqli_fetch_assoc($result)) {
       $fechanacimiento=$row["fechanacimiento"];
       $cinturon=$row["cinturon"];
       $colegio=$row["colegio"];
       $clase=$row["clase"];
       $nombrecolegio=$row["nombrecolegio"];
       $nombreclase=$row["nombreclase"];
       $nombrealumno=$row["nombrealumno"];
       $apellidosalumno=$row["apellidosalumno"];
       $puntuacion=$row["puntuacion"];
       $correopadres=$row["correopadres"];
       $movilpadres=$row["movilpadres"];

  
    }
} else {
    // echo "0 results";
}

$sql = "SELECT * FROM ssa_colegios";

$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) > 0) {
	// output data of each row
$i=0;
	while($row = mysqli_fetch_assoc($result)) {
		$idcolegios[$i]=$row["idcolegio"];
		$nombrecolegios[$i]=$row["nombrecolegio"];

		//echo "<script> console.log('$nombrecolegios[$i]') </script>";

		$i++;
	}
		
} else {
			// echo "0 results";
}
		
		$i=0;		
		$sql = "SELECT * FROM ssa_clases";
		$result = mysqli_query($conn, $sql);
		if (mysqli_num_rows($result) > 0) {
			// output data of each row
		
			while($row = mysqli_fetch_assoc($result)) {
				$idclases[$i]=$row["idclase"];
				$nombreclases[$i]=$row["nombreclase"];
				$i++;
			}
		
		} else {
			// echo "0 results";
		}
		
		$sql = "SELECT ssa_clases.idclase, ssa_clases.nombreclase, ssa_clases.colegio, ssa_colegios.nombrecolegio FROM ssa_clases LEFT JOIN ssa_colegios ON ssa_clases.colegio=ssa_colegios.idcolegio ORDER BY ssa_clases.colegio";

		$result = mysqli_query($conn, $sql);
		if (mysqli_num_rows($result) > 0) {
			// output data of each row
			$i=0;
			echo "<script>";
			while($row = mysqli_fetch_assoc($result)) {

				$idclase=$row["idclase"];
				$nombreclase=$row["nombreclase"];
				$idcolegio=$row["colegio"];
				$nombrecolegio=$row["nombrecolegio"];
				//echo "console.log('$idclase'); ";

				$ids[$i]=$idcolegio."/".$idclase;
				$nombres[$i]=$nombrecolegio."/".$nombreclase;
				
				echo "console.log('$ids[$i]'); ";
				echo "console.log('$nombres[$i]'); ";
				//echo "console.log('$idcolegio'); ";
				//echo "console.log('$nombrecolegio'); ";
		
				$i++;
			}
			echo "</script>";
		
		} else {
				echo "<script> console.log('0 resultado en el join'); </script>";
		}
		
		
		
		
//mysqli_close($conn);
?>  
<script>

function limpia() {

}

function guardaAlumno() {
	var str = document.getElementById("idclasecolegio").value;
    var pos = str.search("/");
 
	  
	$.ajax({
	  type: "POST",
	  url: "guarda_alumno.php",
	  data: 
	{ 
	     uid: <?php echo $uid ?>,
	     nombrealumno: document.getElementById("idnombrealumno").value,
	     apellidosalumno: document.getElementById('idapellidosalumno').value,
	     puntuacion: document.getElementById('idpuntuacion').value,
	     cinturon: document.getElementById('idcinturon').value,
	     colegio: str.slice(0,pos),
	     clase: str.slice(pos+1,str.lenth),
	     correopadres: document.getElementById('idcorreolpadres').value,
	     movilpadres: document.getElementById('idmovilpadres').value,
	     fechanacimiento: document.getElementById('idfechanacimiento').value,
	  }
	}).done(function(o) {
	  console.log('creado'); 
	  console.log(o);
	  document.getElementById("textoventana").innerHTML=o;
	  document.getElementById("ventana").click();
	  // If you want the file to be visible in the browser 
	  // - please modify the callback in javascript. All you
	  // need is to return the url to the file, you just saved 
	  // and than put the image in your browser.
	});
		
}


</script>
</head>

<body>

<?php include 'navegacion.php';?>
    
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Editar Alumno</h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
  
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> <?php echo $nombrealumno." ".$apellidosalumno;?>
                           
                        </div>


                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="row">
 				<div id="divCheckbox" style="display: none;">
					<button id="ventana" class="btn btn-primary btn-lg"
						data-toggle="modal" data-target="#myModal" hidden="hidden">
						Ventana con mensajes</button>
				</div>
                            
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog"
								aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal"
												aria-hidden="true">&times;</button>
											<h4 class="modal-title" id="myModalLabel">Aviso!</h4>
										</div>
										<div class="modal-body" id="textoventana"></div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default"
												data-dismiss="modal">Cerrar</button>
										</div>
									</div>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>
							
							
                            <div class="col-sm-4">
<form action="" method="get" class="form-inline" >
			<div class="form-group">
									<label for="idnombrealumno">Nombre:</label>
									<input type="text" id="idnombrealumno" class="form-control" value="<?php echo $nombrealumno?>" ><br>
									<label for="idapellidosalumno">Apellidos:</label>
									<input type="text" id="idapellidosalumno" size="40" class="form-control" value="<?php echo $apellidosalumno?>" ><br>
									
					
								      
								      <label for="idclasecolegio">Colegio/Clase:</label>

									 <select class="form-control" id="idclasecolegio">							
									
      <?php
$arrlength = count($ids);
for($x = 0; $x < $arrlength; $x++) {
	if( $colegio."/".$clase==$ids[$x])
	{		echo "<option value=".$ids[$x]." selected>".$nombres[$x]."</option>";
	}else {
    echo "<option value=".$ids[$x].">".$nombres[$x]."</option>";
	}


}
?>
								      </select>	<br>
								      
								      
									<label for="idcinturon">Cinturon: (Blanco=1 Blanco/Amarillo=2 ....)</label>
									<input type="text" id="idcinturon" class="form-control" value="<?php echo $cinturon?>" ><br>
									<label for="idpuntuacion">Puntuacion:</label>
									<input type="text" id="idpuntuacion" class="form-control" value="<?php echo $puntuacion?>" ><br>
									<label for="idfechanacimiento">Fecha Nacimiento:</label>
									<input type="date" id="idfechanacimiento" class="form-control" value="<?php echo $fechanacimiento?>" ><br>
									<label for="idcorreolpadres">eMail Padres:</label>
									<input type="text" id="idcorreolpadres" class="form-control" value="<?php echo $correopadres?>" ><br>
									<label for="idmovilpadres">Movil Padres:</label>
									<input type="text" id="idmovilpadres" class="form-control" value="<?php echo $movilpadres?>" ><br>
								</div>
    
<div class="botonestooltip">

									<button type="button" class="btn btn-success btn-circle btn-xl"
										data-toggle="tooltip" data-placement="top" title=""
										data-original-title="Guardar Alumno"
										onClick="guardaAlumno()">
										<i class="fa fa-database"></i>
									</button>
									 <!-- <button type="button" class="btn btn-warning btn-circle btn-xl"
										data-toggle="tooltip" data-placement="top" title=""
										data-original-title="Borrar Seguimiento" onClick="limpia()">
										<i class="fa fa-eraser"></i>
									</button> -->
									
</div>
</form>



                        
                                </div>
                                <!-- /.col-lg-2 (nested) -->
                                                              
                            </div>
                            <!-- /.row -->

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                 
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-8 -->
                
                    <div class="panel panel-default">
                      
                        <!-- /.panel-body -->
                    </div>
                   
                        
                </div>
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    
      <?php include 'pie.php';?>  
      

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

 <script>
    // tooltip demo
    $('.botonestooltip').tooltip({
        selector: "[data-toggle=tooltip]",
        container: "body"
    })

    // popover demo
    $("[data-toggle=popover]")
        .popover()
    </script>

</body>

</html>